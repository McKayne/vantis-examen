//
//  InitialViewController.swift
//  VantisTareaDePrueba
//
//  Created by Nikolay Taran para VANTIS empresa on 21.04.2022.
//

import Foundation
import UIKit
import RealmSwift
import RxSwift

class InitialViewController: LogViewController {
    
    let backgroundImageView = UIImageView()

    var viewModel: InitialViewModel?
    
    var realm: Realm?
    
    let alert = UIAlertController(title: NSLocalizedString("Loading",
                                                           comment: ""),
                                  message: nil,
                                  preferredStyle: .alert)
    
    let disposeBag = DisposeBag()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = InitialViewModelImplementation(initialViewController: self)
        view.backgroundColor = .darkGray
        
        addBackgroundImage()
        
        setupLoadingAlert()

        present(alert, animated: true, completion: { [weak self] in
            self?.checkIfHasValidToken()
        })
    }
    
    private func setupLoadingAlert() {
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.isUserInteractionEnabled = false
        activityIndicator.startAnimating()

        alert.view.addSubview(activityIndicator)
        activityIndicator.centerYAnchor.constraint(equalTo: alert.view.centerYAnchor, constant: 0).isActive = true
        activityIndicator.leftAnchor.constraint(equalTo: alert.view.leftAnchor, constant: 35).isActive = true
    }
    
    private func addBackgroundImage() {
        backgroundImageView.contentMode = .scaleAspectFill
        backgroundImageView.image = UIImage(named: "LogInBackground")
        view.addSubview(backgroundImageView)
        backgroundImageView.translatesAutoresizingMaskIntoConstraints = false
        backgroundImageView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        backgroundImageView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        backgroundImageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        backgroundImageView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
    }
    
    func checkIfHasValidToken() {
        viewModel?.checkIfHasValidToken()
            .subscribe(onSuccess: { hasValidToken in
                DispatchQueue.main.async { [weak self] in
                    if hasValidToken {
                        self?.moveToCatalog()
                    } else {
                        self?.moveToLogIn()
                    }
                }
        }, onFailure: {error in
            DispatchQueue.main.async { [weak self] in
                self?.alert.dismiss(animated: true,
                                   completion: {
                    // oslog not available before ios 14
                    print(String(format: NSLocalizedString("Got Realm error %@", comment: ""), error.localizedDescription))
                })
            }
        }, onDisposed: {
        }).disposed(by: disposeBag)
    }
    
    func moveToLogIn() {
        let logInViewController = LogInViewController()
        alert.dismiss(animated: true, completion: { [weak self] in
            self?.navigationController?.pushViewController(logInViewController, animated: false)
        })
    }
    
    func moveToCatalog() {
        let catalogViewController = CatalogViewController()
        alert.dismiss(animated: true, completion: { [weak self] in
            self?.navigationController?.pushViewController(catalogViewController, animated: false)
        })
    }
}
