//
//  DetailsViewController.swift
//  VantisTareaDePrueba
//
//  Created by Nikolay Taran para VANTIS empresa on 21.04.2022.
//

import Foundation
import UIKit
import OSLog
import AVKit
import RxSwift

class DetailsViewController: LoggedInViewController {
    
    var viewModel: DetailsViewModel?
    
    let stackView = UIStackView()
    
    let coverImageView = UIImageView(), backdropImageView = UIImageView()
    
    let scrollStackView = UIStackView()
    let scrollView = UIScrollView()
    
    var movie: Movie?
    
    var isFavorite: Bool?
    
    let favoriteImageView = UIImageView()
    
    let alert = UIAlertController(title: NSLocalizedString("Loading",
                                                           comment: ""),
                                  message: nil,
                                  preferredStyle: .alert)
    
    let disposeBag = DisposeBag()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isTranslucent = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = DetailsViewModelImplementation(detailsViewController: self)
        
        if let movie = movie {
            navigationItem.title = movie.name
        }
        
        view.backgroundColor = .black
        
        addPoster()
        
        addFavoriteStatus()
        
        addDescription()
        
        addOriginalName()
        
        addVoteAverage()
        
        addVoteCount()
        
        addPopularity()
        
        addFirstAirDate()
        
        addBackdrop()
        
        present(alert, animated: true, completion: { [weak self] in
            if let backdropPath = self?.movie?.backdropPath,
                let id = self?.movie?.id {
                self?.updateBackdrop(backdropPath: backdropPath, id: id)
            } else {
                self?.checkIfFavorite()
            }
        })
    }
    
    private func addFirstAirDate() {
        if let firstAirDate = movie?.firstAirDate {
            let label = UILabel()
            label.textColor = .white
            label.font = UIFont.systemFont(ofSize: 20, weight: .heavy)
            label.text = NSLocalizedString("First Air Date:", comment: "")
            scrollStackView.addArrangedSubview(label)
            
            let valueLabel = UILabel()
            valueLabel.textColor = .white
            valueLabel.font = UIFont.systemFont(ofSize: 20, weight: .bold)
            valueLabel.text = firstAirDate
            scrollStackView.addArrangedSubview(valueLabel)
        }
    }
    
    private func addPopularity() {
        if let popularity = movie?.popularity {
            let label = UILabel()
            label.textColor = .white
            label.font = UIFont.systemFont(ofSize: 20, weight: .heavy)
            label.text = NSLocalizedString("Popularity:", comment: "")
            scrollStackView.addArrangedSubview(label)
            
            let valueLabel = UILabel()
            valueLabel.textColor = .white
            valueLabel.font = UIFont.systemFont(ofSize: 20, weight: .bold)
            valueLabel.text = String(popularity)
            scrollStackView.addArrangedSubview(valueLabel)
        }
    }
    
    private func addVoteCount() {
        if let voteCount = movie?.voteCount {
            let label = UILabel()
            label.textColor = .white
            label.font = UIFont.systemFont(ofSize: 20, weight: .heavy)
            label.text = NSLocalizedString("Vote Count:", comment: "")
            scrollStackView.addArrangedSubview(label)
            
            let valueLabel = UILabel()
            valueLabel.textColor = .white
            valueLabel.font = UIFont.systemFont(ofSize: 20, weight: .bold)
            valueLabel.text = String(voteCount)
            scrollStackView.addArrangedSubview(valueLabel)
        }
    }
    
    private func addVoteAverage() {
        if let voteAverage = movie?.voteAverage {
            let label = UILabel()
            label.textColor = .white
            label.font = UIFont.systemFont(ofSize: 20, weight: .heavy)
            label.text = NSLocalizedString("Vote Average:", comment: "")
            scrollStackView.addArrangedSubview(label)
            
            let valueLabel = UILabel()
            valueLabel.textColor = .white
            valueLabel.font = UIFont.systemFont(ofSize: 20, weight: .bold)
            valueLabel.text = "\(voteAverage) of 10"
            scrollStackView.addArrangedSubview(valueLabel)
        }
    }
    
    private func addOriginalName() {
        if let originalName = movie?.originalName {
            let label = UILabel()
            label.textColor = .white
            label.font = UIFont.systemFont(ofSize: 20, weight: .heavy)
            label.text = NSLocalizedString("Original Name:", comment: "")
            scrollStackView.addArrangedSubview(label)
            
            let valueLabel = UILabel()
            valueLabel.textColor = .white
            valueLabel.font = UIFont.systemFont(ofSize: 20, weight: .bold)
            valueLabel.text = originalName
            scrollStackView.addArrangedSubview(valueLabel)
        }
    }
    
    private func addFavoriteStatus() {
        favoriteImageView.contentMode = .scaleAspectFit
        coverImageView.addSubview(favoriteImageView)
        
        favoriteImageView.translatesAutoresizingMaskIntoConstraints = false
        favoriteImageView.topAnchor.constraint(equalTo: coverImageView.topAnchor, constant: 0).isActive = true
        favoriteImageView.rightAnchor.constraint(equalTo: coverImageView.rightAnchor, constant: -10).isActive = true
        favoriteImageView.widthAnchor.constraint(equalToConstant: 80).isActive = true
        favoriteImageView.heightAnchor.constraint(equalToConstant: 80).isActive = true
    }
    
    private func addBackdrop() {
        if movie?.backdropPath != nil {
            let label = UILabel()
            label.textColor = .white
            label.font = UIFont.systemFont(ofSize: 20,
                                           weight: .heavy)
            label.text = NSLocalizedString("On Backdrop:", comment: "")
            scrollStackView.addArrangedSubview(label)
            
            backdropImageView.contentMode = .scaleAspectFill
            backdropImageView.clipsToBounds = true
            scrollStackView.addArrangedSubview(backdropImageView)
            backdropImageView.translatesAutoresizingMaskIntoConstraints = false
            backdropImageView.heightAnchor.constraint(equalToConstant: 300).isActive = true
            
            backdropImageView.isUserInteractionEnabled = false//true
            backdropImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(playDetailsVideo(_:))))
        }
    }
    
    private func addDescription() {
        let descriptionLabel = UILabel()
        descriptionLabel.textColor = .white
        descriptionLabel.font = UIFont.systemFont(ofSize: 20,
                                                  weight: .bold)
        descriptionLabel.numberOfLines = 0
        descriptionLabel.text = movie?.overview
        descriptionLabel.textAlignment = .justified
        scrollStackView.addArrangedSubview(descriptionLabel)
    }
    
    private func addPoster() {
        view.addSubview(scrollView)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.leftAnchor.constraint(equalTo: view.leftAnchor,
                                         constant: 0).isActive = true
        scrollView.rightAnchor.constraint(equalTo: view.rightAnchor,
                                          constant: 0).isActive = true
        scrollView.topAnchor.constraint(equalTo: view.topAnchor,
                                        constant: 0).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor,
                                           constant: 0).isActive = true
        
        scrollStackView.axis = .vertical
        scrollStackView.spacing = 15
        scrollView.addSubview(scrollStackView)
        
        scrollStackView.translatesAutoresizingMaskIntoConstraints = false
        scrollStackView.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 0).isActive = true
        scrollStackView.rightAnchor.constraint(equalTo: scrollView.rightAnchor, constant: 0).isActive = true
        scrollStackView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0).isActive = true
        scrollStackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0).isActive = true
        scrollStackView.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        
        //
        
        coverImageView.contentMode = .scaleAspectFill
        coverImageView.clipsToBounds = true
        stackView.addArrangedSubview(coverImageView)
        coverImageView.translatesAutoresizingMaskIntoConstraints = false
        coverImageView.heightAnchor.constraint(equalToConstant: 300).isActive = true
        
        coverImageView.isUserInteractionEnabled = true
        coverImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapOnFavorites(_:))))
        
        stackView.axis = .horizontal
        stackView.spacing = 15
        scrollStackView.addArrangedSubview(stackView)
        
        if let id = movie?.id, let posterData = cachedPoster(id: id) {
            let poster = UIImage(data: posterData)
            coverImageView.image = poster
            coverImageView.setNeedsDisplay()
        }
    }
    
    func updateBackdrop(backdropPath: String, id: Int) {
        viewModel?.updateBackdrop(backdropPath: backdropPath, id: id).subscribe(onSuccess: { [weak self] data in
            DispatchQueue.main.async { [weak self] in
                if let data = data, let backdrop = UIImage(data: data) {
                    self?.backdropImageView.image = backdrop
                    self?.backdropImageView.setNeedsDisplay()
                }
                self?.checkIfFavorite()
            }
        }, onFailure: { [weak self] error in
            DispatchQueue.main.async { [weak self] in
                self?.alert.dismiss(animated: true, completion: {
                    self?.logOutAction()
                })
            }
        }, onDisposed: {
        }).disposed(by: disposeBag)
    }
    
    func changeFavoriteStatus(isFavorite: Bool, id: Int) {
        viewModel?.changeFavoriteStatus(isFavorite: isFavorite,
                                        id: id)
            .subscribe(onSuccess: { _ in
                DispatchQueue.main.async { [weak self] in
                    self?.alert.dismiss(animated: true,
                                        completion: {
                        self?.isFavorite = isFavorite
                        
                        self?.favoriteImageView.image = UIImage(named: isFavorite ? "IsFavorite" : "NotFavorite")
                        self?.favoriteImageView.setNeedsDisplay()
                    })
                }
        }, onFailure: {error in
            DispatchQueue.main.async { [weak self] in
                self?.alert.dismiss(animated: true,
                                   completion: {
                    // oslog not available before ios 14
                    print(String(format: NSLocalizedString("Got Realm error %@", comment: ""), error.localizedDescription))
                })
            }
        }, onDisposed: {
        }).disposed(by: disposeBag)
    }
    
    func checkIfFavorite() {
        viewModel?.checkIfFavorite()
            .subscribe(onSuccess: { isFavorite in
                DispatchQueue.main.async { [weak self] in
                    self?.alert.dismiss(animated: true,
                                        completion: {
                        self?.isFavorite = isFavorite
                        
                        self?.favoriteImageView.image = UIImage(named: isFavorite ? "IsFavorite" : "NotFavorite")
                        self?.favoriteImageView.setNeedsDisplay()
                    })
                }
        }, onFailure: {error in
            DispatchQueue.main.async { [weak self] in
                self?.alert.dismiss(animated: true,
                                   completion: {
                    // oslog not available before ios 14
                    print(String(format: NSLocalizedString("Got Realm error %@", comment: ""), error.localizedDescription))
                })
            }
        }, onDisposed: {
        }).disposed(by: disposeBag)
    }
    
    @objc func playDetailsVideo(_ sender: UITapGestureRecognizer) {
        let videoURL = URL(string: "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")
        if let videoURL = videoURL {
            let player = AVPlayer(url: videoURL)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            
            present(playerViewController, animated: true, completion: {
                playerViewController.player?.play()
            })
        }
    }
    
    @objc func tapOnFavorites(_ sender: UITapGestureRecognizer) {
        if let isFavorite = isFavorite, let id = movie?.id {
            present(alert, animated: true, completion: { [weak self] in
                self?.changeFavoriteStatus(isFavorite: !isFavorite, id: id)
            })
        }
    }
    
    func cachedPoster(id: Int) -> Data? {
        let tempDirectory = FileManager.default.temporaryDirectory.appendingPathComponent("VantisExam", isDirectory: true)
        if !FileManager.default.fileExists(atPath: tempDirectory.relativePath) {
            return nil
        }
        
        let posterFile = tempDirectory.appendingPathComponent("poster\(id).jpg", isDirectory: false)
        return try? Data(contentsOf: posterFile)
    }
}
