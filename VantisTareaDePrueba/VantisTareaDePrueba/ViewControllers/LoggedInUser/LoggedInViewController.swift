//
//  LoggedInViewController.swift
//  VantisTareaDePrueba
//
//  Created by Nikolay Taran para VANTIS empresa on 21.04.2022.
//

import Foundation
import UIKit
import RealmSwift

class LoggedInViewController: LogViewController {
    
    var realm: Realm?
    
    func logOutAction() {
        OperationQueue().addOperation { [weak self] in
            do {
                var configuration = Realm.Configuration()
                configuration.deleteRealmIfMigrationNeeded = true
                self?.realm = try Realm(configuration: configuration)
                
                let movies = self?.realm?.objects(Movie.self)
                if let movies = movies {
                    try self?.realm?.write {
                        self?.realm?.delete(movies)
                    }
                }
                
                let favorites = self?.realm?.objects(FavoriteMovie.self)
                if let favorites = favorites {
                    try self?.realm?.write {
                        self?.realm?.delete(favorites)
                    }
                }
                
                let tokens = self?.realm?.objects(Token.self)
                if let tokens = tokens {
                    try self?.realm?.write {
                        self?.realm?.delete(tokens)
                    }
                }
                
                DispatchQueue.main.async {
                    self?.clearImagesCache()
                    self?.moveToLogOut()
                }
            } catch {
                // oslog not available before ios 14
                print(String(format: NSLocalizedString("Got Realm error %@", comment: ""), error.localizedDescription))
            }
        }
    }
    
    func clearImagesCache() {
        let tempDirectory = FileManager.default.temporaryDirectory.appendingPathComponent("VantisExam", isDirectory: true)
        if FileManager.default.fileExists(atPath: tempDirectory.relativePath) {
            try? FileManager.default.removeItem(at: tempDirectory)
        }
    }
    
    func moveToLogOut() {
        let logInViewController = LogInViewController()
        navigationController?.pushViewController(logInViewController,
                                                 animated: true)
    }
}
