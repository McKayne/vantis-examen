//
//  LogInViewController+UITextFieldDelegate.swift
//  VantisTareaDePrueba
//
//  Created by Nikolay Taran para VANTIS empresa on 19.04.2022.
//

import Foundation
import UIKit

extension LogInViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == usernameTextField {
            passwordTextField.becomeFirstResponder()
        } else if textField == passwordTextField {
            logInAction()
        }
        return false
    }
}
