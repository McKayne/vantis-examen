//
//  LogInViewController+Actions.swift
//  VantisTareaDePrueba
//
//  Created by Nikolay Taran para VANTIS empresa on 19.04.2022.
//

import Foundation
import UIKit
import RxSwift

extension LogInViewController {
    
    @objc func logInAction(_ sender: UIButton) {
        logInAction()
    }
    
    func requestToken(username: String, password: String) {
        viewModel?.logIn(username: username,
                         password: password)
            .subscribe(onSuccess: {_ in
            DispatchQueue.main.async { [weak self] in
                self?.alert.dismiss(animated: true,
                                    completion: {
                    self?.moveToCatalog()
                })
            }
        }, onFailure: { [weak self] error in
            DispatchQueue.main.async { [weak self] in
                self?.alert.dismiss(animated: true, completion: {
                    self?.errorLabel.isHidden = false
                    self?.errorLabel.text = error.localizedDescription
                })
            }
        }, onDisposed: {
        }).disposed(by: disposeBag)
    }
    
    @objc func tapAction(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
}
