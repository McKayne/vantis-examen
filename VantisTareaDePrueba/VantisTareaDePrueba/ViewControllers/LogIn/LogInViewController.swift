//
//  SignInViewController.swift
//  VantisTareaDePrueba
//
//  Created by Nikolay Taran para VANTIS empresa on 19.04.2022.
//

import Foundation
import UIKit
import RxSwift
import RealmSwift
import SwiftyJSON

class LogInViewController: LogViewController {
    
    // MARK: - Properties
    
    let backgroundImageView = UIImageView()
    
    let stackView = UIStackView()
    let usernameTextField = UITextField()
    let passwordTextField = UITextField()
    let logInButton = UIButton()
    let errorLabel = UILabel()
    
    var viewModel: LogInViewModel?
    let disposeBag = DisposeBag()
    
    let alert = UIAlertController(title: NSLocalizedString("Loading",
                                                           comment: ""),
                                  message: nil,
                                  preferredStyle: .alert)
    var realm: Realm?
    
    // MARK: - Lifecycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = LogInViewModelImplementation(logInViewController: self)
        
        navigationItem.hidesBackButton = true
        view.backgroundColor = .darkGray
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapAction(_:))))
        
        addBackgroundImage()
        
        addTextFields()
        
        addLogInButton()
        
        addErrorLabel()
        
        addStackView()
        
        setupLoadingAlert()
        
        usernameTextField.becomeFirstResponder()
    }
    
    private func setupLoadingAlert() {
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.isUserInteractionEnabled = false
        activityIndicator.startAnimating()

        alert.view.addSubview(activityIndicator)

        activityIndicator.centerYAnchor.constraint(equalTo: alert.view.centerYAnchor, constant: 0).isActive = true
        activityIndicator.leftAnchor.constraint(equalTo: alert.view.leftAnchor, constant: 35).isActive = true
    }
    
    private func addStackView() {
        stackView.axis = .vertical
        stackView.spacing = 15
        view.addSubview(stackView)
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.leftAnchor.constraint(equalTo: view.leftAnchor,
                                        constant: 30).isActive = true
        stackView.rightAnchor.constraint(equalTo: view.rightAnchor,
                                        constant: -30).isActive = true
        stackView.topAnchor.constraint(equalTo: view.topAnchor,
                                        constant: 150).isActive = true
    }
    
    private func addErrorLabel() {
        errorLabel.textColor = .red
        errorLabel.numberOfLines = 0
        stackView.addArrangedSubview(errorLabel)
        errorLabel.isHidden = true
    }
    
    private func addLogInButton() {
        logInButton.setTitle(NSLocalizedString("Log in", comment: ""), for: .normal)
        logInButton.setTitleColor(.white, for: .normal)
        logInButton.backgroundColor = .lightGray
        logInButton.addTarget(self,
                              action: #selector(logInAction(_:)),
                              for: .touchUpInside)
        stackView.addArrangedSubview(logInButton)
        
        logInButton.translatesAutoresizingMaskIntoConstraints = false
        logInButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    private func addTextFields() {
        usernameTextField.backgroundColor = .white
        passwordTextField.backgroundColor = .white
        
        usernameTextField.placeholder = NSLocalizedString("Username",
                                                          comment: "")
        passwordTextField.placeholder = NSLocalizedString("Password",
                                                          comment: "")
        
        usernameTextField.autocorrectionType = .no
        passwordTextField.isSecureTextEntry = true
        usernameTextField.borderStyle = .roundedRect
        passwordTextField.borderStyle = .roundedRect
        stackView.addArrangedSubview(usernameTextField)
        stackView.addArrangedSubview(passwordTextField)
        
        usernameTextField.translatesAutoresizingMaskIntoConstraints = false
        usernameTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        passwordTextField.translatesAutoresizingMaskIntoConstraints = false
        passwordTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true

        usernameTextField.autocapitalizationType = .none
        usernameTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    private func addBackgroundImage() {
        backgroundImageView.contentMode = .scaleAspectFill
        backgroundImageView.image = UIImage(named: "LogInBackground")
        view.addSubview(backgroundImageView)
        backgroundImageView.translatesAutoresizingMaskIntoConstraints = false
        backgroundImageView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        backgroundImageView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        backgroundImageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        backgroundImageView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
    }
    
    // MARK: - Controller segues
    
    func logInAction() {
        guard
            let username = usernameTextField.text,
            let password = passwordTextField.text
        else {
            return
        }
        
        if username.count == 0 {
            usernameTextField.becomeFirstResponder()
        } else if password.count == 0 {
            passwordTextField.becomeFirstResponder()
        } else {
            present(alert, animated: true, completion: { [weak self] in
                self?.requestToken(username: username,
                                   password: password)
            })
        }
    }
    
    func moveToCatalog() {
        let catalogViewController = CatalogViewController()
        navigationController?.pushViewController(catalogViewController,
                                                 animated: true)
    }
}
