//
//  ProfileViewController+CollectionView.swift
//  VantisTareaDePrueba
//
//  Created by Nikolay Taran para VANTIS empresa on 22.04.2022.
//

import Foundation
import UIKit

extension ProfileViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return movies.count % 2 == 0 ? movies.count / 2 : movies.count / 2 + 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        let sectionsTotal = movies.count % 2 == 0 ? movies.count / 2 : movies.count / 2 + 1
        if section < sectionsTotal - 1 {
            return 2
        } else {
            return movies.count % 2 == 0 ? 2 : 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let catalogCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CatalogViewCell", for: indexPath)
        
        if let catalogCell = catalogCell as? CatalogViewCell {
            let movie = movies[indexPath.section * 2 + indexPath.row]
            
            let poster: UIImage?
            if let id = movie.id, let posterData = viewModel?.cachedPoster(id: id) {
                poster = UIImage(data: posterData)
            } else {
                poster = nil
            }
            
            catalogCell.updateCell(poster: poster, movie: movie)
        }
        
        return catalogCell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {
        let detailsViewController = DetailsViewController()
        let movie = movies[indexPath.section * 2 + indexPath.row]
        detailsViewController.movie = movie
        navigationController?.pushViewController(detailsViewController,
                                                 animated: true)
    }
}
