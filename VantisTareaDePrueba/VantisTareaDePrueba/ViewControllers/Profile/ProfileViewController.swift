//
//  ProfileViewController.swift
//  VantisTareaDePrueba
//
//  Created by Nikolay Taran para VANTIS empresa on 21.04.2022.
//

import Foundation
import UIKit
import RealmSwift
import RxSwift
import SwiftyJSON

class ProfileViewController: LoggedInViewController {
    
    var viewModel: ProfileViewModel?
    
    let alert = UIAlertController(title: NSLocalizedString("Loading",
                                                           comment: ""),
                                  message: nil,
                                  preferredStyle: .alert)
    
    let stackView = UIStackView()
    
    let avatarImageView = UIImageView()
    let usernameLabel = UILabel()
    
    let scrollStackView = UIStackView()
    let scrollView = UIScrollView()
    
    let flowLayout = UICollectionViewFlowLayout()
    var collectionView: UICollectionView?
    
    let disposeBag = DisposeBag()
    
    var collectionViewHeightConstraint: NSLayoutConstraint?
    
    var movies: [Movie] = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isTranslucent = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = ProfileViewModelImplementation(profileViewController: self)
        
        navigationItem.title = NSLocalizedString("Profile", comment: "")
        view.backgroundColor = .black

        addScrollView()
        
        addAvatar()
        
        addFavoriteShows()
        
        setupLoadingAlert()
        
        present(alert, animated: true, completion: { [weak self] in
            self?.loadProfileData()
        })
    }
    
    private func addScrollView() {
        view.addSubview(scrollView)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.leftAnchor.constraint(equalTo: view.leftAnchor,
                                         constant: 0).isActive = true
        scrollView.rightAnchor.constraint(equalTo: view.rightAnchor,
                                          constant: 0).isActive = true
        scrollView.topAnchor.constraint(equalTo: view.topAnchor,
                                        constant: 10).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor,
                                           constant: 0).isActive = true
        
        scrollStackView.axis = .vertical
        scrollStackView.spacing = 15
        scrollView.addSubview(scrollStackView)
        
        scrollStackView.translatesAutoresizingMaskIntoConstraints = false
        scrollStackView.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 0).isActive = true
        scrollStackView.rightAnchor.constraint(equalTo: scrollView.rightAnchor, constant: 0).isActive = true
        scrollStackView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0).isActive = true
        scrollStackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0).isActive = true
        scrollStackView.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
    }
    
    private func setupLoadingAlert() {
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.isUserInteractionEnabled = false
        activityIndicator.startAnimating()

        alert.view.addSubview(activityIndicator)

        activityIndicator.centerYAnchor.constraint(equalTo: alert.view.centerYAnchor, constant: 0).isActive = true
        activityIndicator.leftAnchor.constraint(equalTo: alert.view.leftAnchor, constant: 35).isActive = true
    }
    
    private func addFavoriteShows() {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 20,
                                       weight: .heavy)
        label.text = NSLocalizedString("Favorite Shows",
                                                  comment: "")
        scrollStackView.addArrangedSubview(label)
        
        //
        
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0,
                                               bottom: 10, right: 0)
        flowLayout.itemSize = CGSize(width: Int(view.frame.width / 2) - 5,
                                     height: Int(UIScreen.main.bounds.height) / 3)
        
        collectionView = UICollectionView(frame: .zero,
                                          collectionViewLayout: flowLayout)
        if let collectionView = collectionView {
            collectionView.isScrollEnabled = false
            
            collectionView.register(CatalogViewCell.self,
                                    forCellWithReuseIdentifier: "CatalogViewCell")
            collectionView.backgroundColor = .black
            
            collectionView.delegate = self
            collectionView.dataSource = self
            scrollStackView.addArrangedSubview(collectionView)
            
            collectionView.translatesAutoresizingMaskIntoConstraints = false
            
            collectionViewHeightConstraint = collectionView.heightAnchor.constraint(equalToConstant: 0)
            collectionViewHeightConstraint?.isActive = true
        }
    }
    
    private func addAvatar() {
        let leftView = UIView()
        stackView.addArrangedSubview(leftView)
        leftView.translatesAutoresizingMaskIntoConstraints = false
        leftView.widthAnchor.constraint(equalToConstant: 15).isActive = true
        
        avatarImageView.contentMode = .scaleAspectFill
        stackView.addArrangedSubview(avatarImageView)
        avatarImageView.translatesAutoresizingMaskIntoConstraints = false
        avatarImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        avatarImageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        avatarImageView.clipsToBounds = true
        avatarImageView.layer.cornerRadius = 50
        
        usernameLabel.textColor = .white
        usernameLabel.font = UIFont.systemFont(ofSize: 20,
                                               weight: .heavy)
        stackView.addArrangedSubview(usernameLabel)
        
        let rightView = UIView()
        stackView.addArrangedSubview(rightView)
        rightView.translatesAutoresizingMaskIntoConstraints = false
        rightView.widthAnchor.constraint(equalToConstant: 15).isActive = true
        
        stackView.axis = .horizontal
        stackView.spacing = 15
        scrollStackView.addArrangedSubview(stackView)
    }
    
    func loadProfileData() {
        viewModel?.updateProfile().subscribe(onSuccess: { [weak self] data in
            DispatchQueue.main.async { [weak self] in
                self?.alert.dismiss(animated: true,
                                    completion: {
                    if let data = data, let avatar = UIImage(data: data) {
                        self?.avatarImageView.image = avatar
                        self?.avatarImageView.setNeedsDisplay()
                    }
                })
            }
        }, onFailure: { [weak self] error in
            DispatchQueue.main.async { [weak self] in
                self?.alert.dismiss(animated: true, completion: {
                    self?.logOutAction()
                })
            }
        }, onDisposed: {
        }).disposed(by: disposeBag)
    }
}
