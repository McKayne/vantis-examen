//
//  CatalogViewController.swift
//  VantisTareaDePrueba
//
//  Created by Nikolay Taran para VANTIS empresa on 21.04.2022.
//

import Foundation
import UIKit
import RealmSwift
import RxSwift

class CatalogViewController: LoggedInViewController {
    
    var viewModel: CatalogViewModel?
    
    let disposeBag = DisposeBag()
    
    let segmentedControl = UISegmentedControl()
    
    let flowLayout = UICollectionViewFlowLayout()
    var collectionView: UICollectionView?
    
    let alert = UIAlertController(title: NSLocalizedString("Loading",
                                                           comment: ""),
                                  message: nil,
                                  preferredStyle: .alert)
    
    var movies: [Movie] = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = CatalogViewModelImplementation(catalogViewController: self)
        
        navigationItem.title = NSLocalizedString("TV Shows", comment: "")
        navigationItem.hidesBackButton = true
        view.backgroundColor = .black
        
        let profileButton = UIBarButtonItem(image: UIImage(named: "CatalogMenu")?.imageResized(to: CGSize(width: 64, height: 64)), style: .plain, target: self, action: #selector(presentBottomMenu(_:)))
        navigationItem.rightBarButtonItem = profileButton
        
        addSegmentedControl()
        
        addCollectionView()
        
        setupLoadingAlert()
        
        present(alert, animated: true, completion: { [weak self] in
            self?.showPopular()
        })
    }
    
    private func addSegmentedControl() {
        segmentedControl.setTitleTextAttributes([.foregroundColor: UIColor.white], for: .normal)
        segmentedControl.setTitleTextAttributes([.foregroundColor: UIColor.black], for: .selected)
        
        segmentedControl.insertSegment(withTitle: NSLocalizedString("Popular", comment: ""),
                                       at: 0,
                                       animated: false)
        segmentedControl.insertSegment(withTitle: NSLocalizedString("Top Rated", comment: ""),
                                       at: 1,
                                       animated: false)
        segmentedControl.insertSegment(withTitle: NSLocalizedString("On TV", comment: ""),
                                       at: 2,
                                       animated: false)
        segmentedControl.insertSegment(withTitle: NSLocalizedString("Airing Today", comment: ""),
                                       at: 3,
                                       animated: false)
        view.addSubview(segmentedControl)
        
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        segmentedControl.leftAnchor.constraint(equalTo: view.leftAnchor,
                                               constant: 10).isActive = true
        segmentedControl.rightAnchor.constraint(equalTo: view.rightAnchor,
                                                constant: -10).isActive = true
        segmentedControl.topAnchor.constraint(equalTo: view.topAnchor,
                                              constant: 10).isActive = true
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.addTarget(self,
                                   action: #selector(typeChange(_:)),
                                   for: .valueChanged)
    }
    
    private func addCollectionView() {
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0,
                                               bottom: 10, right: 0)
        flowLayout.itemSize = CGSize(width: Int(view.frame.width / 2) - 5,
                                     height: Int(UIScreen.main.bounds.height) / 3)
        
        collectionView = UICollectionView(frame: .zero,
                                          collectionViewLayout: flowLayout)
        if let collectionView = collectionView {
            collectionView.register(CatalogViewCell.self,
                                    forCellWithReuseIdentifier: "CatalogViewCell")
            collectionView.backgroundColor = .black
            
            collectionView.delegate = self
            collectionView.dataSource = self
            view.addSubview(collectionView)
            
            collectionView.translatesAutoresizingMaskIntoConstraints = false
            collectionView.leftAnchor.constraint(equalTo: view.leftAnchor,
                                                 constant: 0).isActive = true
            collectionView.rightAnchor.constraint(equalTo: view.rightAnchor,
                                                  constant: 0).isActive = true
            collectionView.topAnchor.constraint(equalTo: segmentedControl.bottomAnchor, constant: 10).isActive = true
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        }
    }
    
    private func setupLoadingAlert() {
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.isUserInteractionEnabled = false
        activityIndicator.startAnimating()

        alert.view.addSubview(activityIndicator)
        activityIndicator.centerYAnchor.constraint(equalTo: alert.view.centerYAnchor, constant: 0).isActive = true
        activityIndicator.leftAnchor.constraint(equalTo: alert.view.leftAnchor, constant: 35).isActive = true
    }
    
    func showPopular() {
        viewModel?.showPopular().subscribe(onSuccess: {movies in
            DispatchQueue.main.async { [weak self] in
                self?.alert.dismiss(animated: true,
                                    completion: {
                    self?.movies = movies
                    self?.collectionView?.reloadData()
                })
            }
        }, onFailure: { [weak self] error in
            DispatchQueue.main.async { [weak self] in
                self?.alert.dismiss(animated: true, completion: {
                    self?.logOutAction()
                })
            }
        }, onDisposed: {
        }).disposed(by: disposeBag)
    }
    
    func showTopRated() {
        viewModel?.showTopRated().subscribe(onSuccess: {movies in
            DispatchQueue.main.async { [weak self] in
                self?.alert.dismiss(animated: true,
                                    completion: {
                    self?.movies = movies
                    self?.collectionView?.reloadData()
                })
            }
        }, onFailure: { [weak self] error in
            DispatchQueue.main.async { [weak self] in
                self?.alert.dismiss(animated: true, completion: {
                    self?.logOutAction()
                })
            }
        }, onDisposed: {
        }).disposed(by: disposeBag)
    }
    
    func showOnTV() {
        viewModel?.showOnTV().subscribe(onSuccess: {movies in
            DispatchQueue.main.async { [weak self] in
                self?.alert.dismiss(animated: true,
                                    completion: {
                    self?.movies = movies
                    self?.collectionView?.reloadData()
                })
            }
        }, onFailure: { [weak self] error in
            DispatchQueue.main.async { [weak self] in
                self?.alert.dismiss(animated: true, completion: {
                    self?.logOutAction()
                })
            }
        }, onDisposed: {
        }).disposed(by: disposeBag)
    }
    
    func showAiringToday() {
        viewModel?.showAiringToday().subscribe(onSuccess: {movies in
            DispatchQueue.main.async { [weak self] in
                self?.alert.dismiss(animated: true,
                                    completion: {
                    self?.movies = movies
                    self?.collectionView?.reloadData()
                })
            }
        }, onFailure: { [weak self] error in
            DispatchQueue.main.async { [weak self] in
                self?.alert.dismiss(animated: true, completion: {
                    self?.logOutAction()
                })
            }
        }, onDisposed: {
        }).disposed(by: disposeBag)
    }
    
    @objc func typeChange(_ sender: UISegmentedControl) {
        present(alert, animated: true, completion: { [weak self] in
            switch sender.selectedSegmentIndex {
            case 0:
                self?.showPopular()
            case 1:
                self?.showTopRated()
            case 2:
                self?.showOnTV()
            case 3:
                self?.showAiringToday()
            default:
                break
            }
        })
    }
    
    @objc func presentBottomMenu(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: nil,
                                                message: NSLocalizedString("What do you want to do?", comment: ""),
                                                preferredStyle: .actionSheet)
        let profileAction = UIAlertAction(title: NSLocalizedString("View Profile", comment: ""), style: .default) { (result : UIAlertAction) -> Void in
            self.moveToProfile()
        }
        let logOutAction = UIAlertAction(title: NSLocalizedString("Log out", comment: ""), style: .destructive) { (result : UIAlertAction) -> Void in
            self.logOutAction()
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel) { (result : UIAlertAction) -> Void in
            //action when pressed button
        }
        alertController.addAction(profileAction)
        alertController.addAction(logOutAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func moveToProfile() {
        let profileViewController = ProfileViewController()
        navigationController?.pushViewController(profileViewController,
                                                 animated: true)
    }
}
