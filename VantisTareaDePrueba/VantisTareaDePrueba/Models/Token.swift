//
//  Token.swift
//  VantisTareaDePrueba
//
//  Created by Nikolay Taran para VANTIS empresa on 21.04.2022.
//

import Foundation
import RealmSwift

class Token: Object {
    @Persisted var username: String?
    @Persisted var requestToken: String?
    @Persisted var sessionID: String?
}
