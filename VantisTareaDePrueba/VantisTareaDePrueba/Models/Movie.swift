//
//  Movie.swift
//  VantisTareaDePrueba
//
//  Created by Nikolay Taran para VANTIS empresa on 21.04.2022.
//

import Foundation
import RealmSwift

class Movie: Object {
    @Persisted var backdropPath: String?
    @Persisted var firstAirDate: String?
    @Persisted var id: Int?
    @Persisted var name: String?
    @Persisted var originalLanguage: String?
    @Persisted var originalName: String?
    @Persisted var overview: String?
    @Persisted var popularity: Double?
    @Persisted var posterPath: String?
    @Persisted var voteAverage: Double?
    @Persisted var voteCount: Int?
}
