//
//  InitialViewModelImplementation.swift
//  VantisTareaDePrueba
//
//  Created by Nikolay Taran para VANTIS empresa on 21.04.2022.
//

import Foundation
import RealmSwift
import RxSwift

class InitialViewModelImplementation: NSObject, InitialViewModel {
    
    let initialViewController: InitialViewController
    
    init(initialViewController: InitialViewController) {
        self.initialViewController = initialViewController
    }
    
    func checkIfHasValidToken() -> Single<Bool> {
        return Single<Bool>.create { single in
            OperationQueue().addOperation { [weak self] in
                do {
                    var configuration = Realm.Configuration()
                    configuration.deleteRealmIfMigrationNeeded = true
                    self?.initialViewController.realm = try Realm(configuration: configuration)
                    
                    let token = self?.initialViewController.realm?.objects(Token.self).first
                    if token != nil {
                        single(.success(true))
                    } else {
                        single(.success(false))
                    }
                } catch {
                    single(.failure(error))
                }
            }
            
            return Disposables.create()
        }
    }
}
