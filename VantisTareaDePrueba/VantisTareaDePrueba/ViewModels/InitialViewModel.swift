//
//  InitialViewModel.swift
//  VantisTareaDePrueba
//
//  Created by Nikolay Taran para VANTIS empresa on 21.04.2022.
//

import Foundation
import RxSwift

protocol InitialViewModel {
    func checkIfHasValidToken() -> Single<Bool>
}
