//
//  LogInViewModel.swift
//  VantisTareaDePrueba
//
//  Created by Nikolay Taran para VANTIS empresa on 21.04.2022.
//

import Foundation
import RxSwift

protocol LogInViewModel {
    func logIn(username: String, password: String) -> Single<Bool>
}
