//
//  ProfileViewModelImplementation.swift
//  VantisTareaDePrueba
//
//  Created by Nikolay Taran para VANTIS empresa on 21.04.2022.
//

import Foundation
import RxSwift
import RealmSwift
import SwiftyJSON

class ProfileViewModelImplementation: NSObject, ProfileViewModel {
    
    // MARK: - Properties
    
    let profileViewController: ProfileViewController
    
    private let disposeBag = DisposeBag()
    
    private var profileSingle: ((Result<Data?, Error>) -> Void)?
    
    init(profileViewController: ProfileViewController) {
        self.profileViewController = profileViewController
    }
    
    func updateProfile() -> Single<Data?> {
        return Single<Data?>.create { [weak self] single in
            self?.profileSingle = single
            self?.loadProfileData()
            return Disposables.create()
        }
    }
    
    private func loadProfileData() {
        OperationQueue().addOperation { [weak self] in
            do {
                var configuration = Realm.Configuration()
                configuration.deleteRealmIfMigrationNeeded = true
                self?.profileViewController.realm = try Realm(configuration: configuration)
                
                let token = self?.profileViewController.realm?.objects(Token.self).first
                if let token = token, let username = token.username {
                    DispatchQueue.main.async {
                        self?.profileViewController.usernameLabel.text = "@\(username)"
                    }
                }
                
                let movies = self?.profileViewController.realm?.objects(Movie.self)
                let favorites = self?.profileViewController.realm?.objects(FavoriteMovie.self)
                if let movies = movies, let favorites = favorites {
                    self?.profileViewController.movies.removeAll()
                    
                    for movie in movies {
                        var isFavorite = false
                        for favorite in favorites {
                            if movie.id == favorite.id {
                                isFavorite = true
                                break
                            }
                        }
                        
                        if isFavorite, let clone = self?.clone(existingMovie: movie) {
                            self?.profileViewController.movies.append(clone)
                        }
                    }
                    
                    if let movies = self?.profileViewController.movies {
                        let numberOfSections = movies.count % 2 == 0 ? movies.count / 2 : movies.count / 2 + 1
                    
                        DispatchQueue.main.async {
                            let sectionHeight = UIScreen.main.bounds.height / 3 + 10
                            self?.profileViewController.collectionViewHeightConstraint?.constant = CGFloat(CGFloat(numberOfSections) * sectionHeight)
                            self?.profileViewController.collectionView?.setNeedsLayout()
                        
                            self?.profileViewController.collectionView?.reloadData()
                        }
                    }
                }
                
                if let data = self?.cachedAvatar(), UIImage(data: data) != nil {
                    self?.profileSingle?(.success(data))
                } else {
                    if let token = token, let sessionID = token.sessionID {
                        self?.requestAvatarURL(sessionID: sessionID)
                    }
                }
            } catch {
                self?.profileSingle?(.failure(error))
                // oslog not available before ios 14
                //print(String(format: NSLocalizedString("Got Realm error %@", comment: ""), error.localizedDescription))
            }
        }
    }
    
    func requestAvatarURL(sessionID: String) {
        self.loadAvatarURL(sessionID: sessionID).subscribe(onSuccess: {avatarPath in
            DispatchQueue.main.async { [weak self] in
                let avatarURL = "https://image.tmdb.org/t/p/original" + avatarPath
                self?.requestAvatar(avatarURL: avatarURL)
            }
        }, onFailure: { [weak self] error in
            self?.profileSingle?(.failure(error))
        }, onDisposed: {
        }).disposed(by: disposeBag)
    }
    
    func requestAvatar(avatarURL: String) {
        self.loadAvatar(avatarURL: avatarURL).subscribe(onSuccess: {data in
            DispatchQueue.main.async { [weak self] in
                if UIImage(data: data) != nil {
                    self?.saveAvatar(data: data)
                    
                    // oslog not available before ios 14
                    //self?.logger.debug("image loaded")
                    print("image loaded")
                } else {
                    // oslog not available before ios 14
                    print("no image")
                    //self?.logger.debug("no image")
                }
                
                self?.profileSingle?(.success(data))
            }
        }, onFailure: { [weak self] error in
            self?.profileSingle?(.failure(error))
        }, onDisposed: {
        }).disposed(by: disposeBag)
    }
    
    func cachedAvatar() -> Data? {
        let tempDirectory = FileManager.default.temporaryDirectory.appendingPathComponent("VantisExam", isDirectory: true)
        if !FileManager.default.fileExists(atPath: tempDirectory.relativePath) {
            return nil
        }
        
        let avatarFile = tempDirectory.appendingPathComponent("avatar.jpg", isDirectory: false)
        return try? Data(contentsOf: avatarFile)
    }
    
    func saveAvatar(data: Data) {
        do {
            let tempDirectory = FileManager.default.temporaryDirectory.appendingPathComponent("VantisExam", isDirectory: true)
            if !FileManager.default.fileExists(atPath: tempDirectory.relativePath) {
                try FileManager.default.createDirectory(at: tempDirectory,
                                                         withIntermediateDirectories: true,
                                                         attributes: nil)
            }
            
            let avatarFile = tempDirectory.appendingPathComponent("avatar.jpg", isDirectory: false)
            try data.write(to: avatarFile)
            
            // oslog not available before ios 14
            print("Avatar saved successfully")
            //logger.debug("Avatar saved successfully")
        } catch {
            // oslog not available before ios 14
            print(error)
        }
    }
    
    func loadAvatar(avatarURL: String) -> Single<Data> {
        return Single<Data>.create { single in
            if let url = URL(string: avatarURL) {
                let task = URLSession.shared.dataTask(with: url) { data, response, error in
                    if let data = data {
                        single(.success(data))
                    } else {
                        let error = NSError(domain: "com.vantis.error",
                                            code: 0,
                                            userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Empty data", comment: "")])
                        single(.failure(error))
                    }
                }
                task.resume()
            } else {
                let error = NSError(domain: "com.vantis.error",
                                    code: 0,
                                    userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Wrong URL", comment: "")])
                single(.failure(error))
            }
            
            return Disposables.create()
        }
    }
    
    func loadAvatarURL(sessionID: String) -> Single<String> {
        let apiKey = Bundle.main.object(forInfoDictionaryKey: "TMDB API Key") as? String
        return Single<String>.create { single in
            if let apiKey = apiKey {
                let tokenURL = "https://api.themoviedb.org/3/account?api_key=\(apiKey)&session_id=\(sessionID)"
                
                if let url = URL(string: tokenURL) {
                    let task = URLSession.shared.dataTask(with: url) { data, response, error in
                        if let data = data, let json = try? JSON(data: data) {
                            
                            let avatar = json["avatar"]
                            let tmdb = avatar["tmdb"]
                            let avatarPath = tmdb["avatar_path"].string
                            if let avatarPath = avatarPath {
                                single(.success(avatarPath))
                            } else {
                                let error = NSError(domain: "com.vantis.error",
                                                    code: 0,
                                                    userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Empty data", comment: "")])
                                single(.failure(error))
                            }
                        } else {
                            let error = NSError(domain: "com.vantis.error",
                                                code: 0,
                                                userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Empty data", comment: "")])
                            single(.failure(error))
                        }
                    }
                    task.resume()
                } else {
                    let error = NSError(domain: "com.vantis.error",
                                        code: 0,
                                        userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Wrong URL", comment: "")])
                    single(.failure(error))
                }
            } else {
                let error = NSError(domain: "com.vantis.error",
                                    code: 0,
                                    userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Wrong URL", comment: "")])
                single(.failure(error))
            }
            
            return Disposables.create()
        }
    }
    
    private func clone(existingMovie: Movie) -> Movie {
        let movie = Movie()
        
        movie.backdropPath = existingMovie.backdropPath
        movie.firstAirDate = existingMovie.firstAirDate
        movie.id = existingMovie.id
        movie.name = existingMovie.name
        movie.originalLanguage = existingMovie.originalLanguage
        movie.originalName = existingMovie.originalName
        movie.overview = existingMovie.overview
        movie.popularity = existingMovie.popularity
        movie.posterPath = existingMovie.posterPath
        movie.voteAverage = existingMovie.voteAverage
        movie.voteCount = existingMovie.voteCount
        
        return movie
    }
    
    func cachedPoster(id: Int) -> Data? {
        let tempDirectory = FileManager.default.temporaryDirectory.appendingPathComponent("VantisExam", isDirectory: true)
        if !FileManager.default.fileExists(atPath: tempDirectory.relativePath) {
            return nil
        }
        
        let posterFile = tempDirectory.appendingPathComponent("poster\(id).jpg", isDirectory: false)
        return try? Data(contentsOf: posterFile)
    }
}
