//
//  LogInViewModelImplementation.swift
//  VantisTareaDePrueba
//
//  Created by Nikolay Taran para VANTIS empresa on 21.04.2022.
//

import Foundation
import RxSwift
import SwiftyJSON
import RealmSwift

class LogInViewModelImplementation: NSObject, LogInViewModel {
    
    // MARK: - Properties
    
    let logInViewController: LogInViewController
    
    private let disposeBag = DisposeBag()
    
    private var authSingle: ((Result<Bool, Error>) -> Void)?
    
    init(logInViewController: LogInViewController) {
        self.logInViewController = logInViewController
    }
    
    // MARK: - Requesting token for TMDB service using API key
    
    func logIn(username: String, password: String) -> Single<Bool> {
        return Single<Bool>.create { [weak self] single in
            self?.authSingle = single
            self?.requestToken(username: username, password: password)
            return Disposables.create()
        }
    }
    
    private func requestToken(username: String, password: String) {
        loadRequestToken().subscribe(onSuccess: {requestToken in
            DispatchQueue.main.async { [weak self] in
                self?.validateToken(requestToken: requestToken, username: username, password: password)
            }
        }, onFailure: { [weak self] error in
            self?.authSingle?(.failure(error))
        }, onDisposed: {
        }).disposed(by: disposeBag)
    }
    
    private func loadRequestToken() -> Single<String> {
        let apiKey = Bundle.main.object(forInfoDictionaryKey: "TMDB API Key") as? String
        return Single<String>.create { single in
            if let apiKey = apiKey {
                let tokenURL = "https://api.themoviedb.org/3/authentication/token/new?api_key=" + apiKey
                
                if let url = URL(string: tokenURL) {
                    let task = URLSession.shared.dataTask(with: url) { data, response, error in
                        if let data = data, let json = try? JSON(data: data) {
                            let requestToken = json["request_token"].string
                            let statusMessage = json["status_message"].string
                            if let requestToken = requestToken {
                                single(.success(requestToken))
                            } else if let statusMessage = statusMessage {
                                let error = NSError(domain: "com.vantis.error",
                                                    code: 0,
                                                    userInfo: [NSLocalizedDescriptionKey: statusMessage])
                                single(.failure(error))
                            } else {
                                let error = NSError(domain: "com.vantis.error",
                                                    code: 0,
                                                    userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Empty data", comment: "")])
                                single(.failure(error))
                            }
                        } else if let error = error {
                            single(.failure(error))
                        } else {
                            let error = NSError(domain: "com.vantis.error",
                                                code: 0,
                                                userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Empty data", comment: "")])
                            single(.failure(error))
                        }
                    }
                    task.resume()
                } else {
                    let error = NSError(domain: "com.vantis.error",
                                        code: 0,
                                        userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Wrong URL", comment: "")])
                    single(.failure(error))
                }
            } else {
                let error = NSError(domain: "com.vantis.error",
                                    code: 0,
                                    userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Wrong URL", comment: "")])
                single(.failure(error))
            }
            
            return Disposables.create()
        }
    }
    
    // MARK: - Request token validation with username and password
    
    private func validateToken(requestToken: String,
                       username: String,
                       password: String) {
        validateTokenWithCredentials(requestToken: requestToken,
                                     username: username,
                                     password: password).subscribe(onSuccess: {_ in
            DispatchQueue.main.async { [weak self] in
                self?.requestSessionID(requestToken: requestToken, username: username, password: password)
            }
        }, onFailure: { [weak self] error in
            self?.authSingle?(.failure(error))
        }, onDisposed: {
        }).disposed(by: disposeBag)
    }
    
    private func validateTokenWithCredentials(requestToken: String,
                                      username: String,
                                      password: String) -> Single<Bool> {
        let apiKey = Bundle.main.object(forInfoDictionaryKey: "TMDB API Key") as? String
        return Single<Bool>.create { single in
            if let apiKey = apiKey {
                let tokenURL = "https://api.themoviedb.org/3/authentication/token/validate_with_login?api_key=" + apiKey
                
                if let url = URL(string: tokenURL) {
                    let jsonDict: [String: Any] = ["username": username,
                                                   "password": password,
                                                   "request_token": requestToken]
                    let json = JSON(jsonDict)
                    var request = URLRequest(url: url)
                    request.setValue("application/json",
                                     forHTTPHeaderField: "Content-Type")
                    request.httpMethod = "POST"
                    request.httpBody = try? json.rawData()
                    
                    let task = URLSession.shared.dataTask(with: request) { data, response, error in
                        if let data = data, let json = try? JSON(data: data) {
                            let success = json["success"].bool
                            let statusMessage = json["status_message"].string
                            if let success = success, success {
                                single(.success(true))
                            } else if let statusMessage = statusMessage {
                                let error = NSError(domain: "com.vantis.error",
                                                    code: 0,
                                                    userInfo: [NSLocalizedDescriptionKey: statusMessage])
                                single(.failure(error))
                            } else {
                                let error = NSError(domain: "com.vantis.error",
                                                    code: 0,
                                                    userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Empty data", comment: "")])
                                single(.failure(error))
                            }
                        } else if let error = error {
                            single(.failure(error))
                        } else {
                            let error = NSError(domain: "com.vantis.error",
                                                code: 0,
                                                userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Empty data", comment: "")])
                            single(.failure(error))
                        }
                    }
                    task.resume()
                } else {
                    let error = NSError(domain: "com.vantis.error",
                                        code: 0,
                                        userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Wrong URL", comment: "")])
                    single(.failure(error))
                }
            } else {
                let error = NSError(domain: "com.vantis.error",
                                    code: 0,
                                    userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Wrong URL", comment: "")])
                single(.failure(error))
            }
            
            return Disposables.create()
        }
    }
    
    // MARK: - Requesting session ID to use it in requests
    
    private func requestSessionID(requestToken: String,
                          username: String,
                          password: String) {
        loadSessionID(requestToken: requestToken).subscribe(onSuccess: {sessionID in
            DispatchQueue.main.async { [weak self] in
                self?.saveTokenAndContinue(username: username,
                                           requestToken: requestToken,
                                           sessionID: sessionID)
            }
        }, onFailure: { [weak self] error in
            self?.authSingle?(.failure(error))
        }, onDisposed: {
        }).disposed(by: disposeBag)
    }
    
    private func loadSessionID(requestToken: String) -> Single<String> {
        let apiKey = Bundle.main.object(forInfoDictionaryKey: "TMDB API Key") as? String
        return Single<String>.create { single in
            if let apiKey = apiKey {
                let tokenURL = "https://api.themoviedb.org/3/authentication/session/new?api_key=" + apiKey
                
                if let url = URL(string: tokenURL) {
                    let jsonDict: [String: Any] = ["request_token": requestToken]
                    let json = JSON(jsonDict)
                    var request = URLRequest(url: url)
                    request.setValue("application/json",
                                     forHTTPHeaderField: "Content-Type")
                    request.httpMethod = "POST"
                    request.httpBody = try? json.rawData()
                    
                    let task = URLSession.shared.dataTask(with: request) { data, response, error in
                        if let data = data, let json = try? JSON(data: data) {
                            let sessionID = json["session_id"].string
                            let statusMessage = json["status_message"].string
                            if let sessionID = sessionID {
                                single(.success(sessionID))
                            } else if let statusMessage = statusMessage {
                                let error = NSError(domain: "com.vantis.error",
                                                    code: 0,
                                                    userInfo: [NSLocalizedDescriptionKey: statusMessage])
                                single(.failure(error))
                            } else {
                                let error = NSError(domain: "com.vantis.error",
                                                    code: 0,
                                                    userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Empty data", comment: "")])
                                single(.failure(error))
                            }
                        } else if let error = error {
                            single(.failure(error))
                        } else {
                            let error = NSError(domain: "com.vantis.error",
                                                code: 0,
                                                userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Empty data", comment: "")])
                            single(.failure(error))
                        }
                    }
                    task.resume()
                } else {
                    let error = NSError(domain: "com.vantis.error",
                                        code: 0,
                                        userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Wrong URL", comment: "")])
                    single(.failure(error))
                }
            } else {
                let error = NSError(domain: "com.vantis.error",
                                    code: 0,
                                    userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Wrong URL", comment: "")])
                single(.failure(error))
            }
            
            return Disposables.create()
        }
    }
    
    // MARK: - Saving important credentials to cache
    
    private func saveTokenAndContinue(username: String,
                              requestToken: String,
                              sessionID: String) {
        OperationQueue().addOperation { [weak self] in
            do {
                var configuration = Realm.Configuration()
                configuration.deleteRealmIfMigrationNeeded = true
                self?.logInViewController.realm = try Realm(configuration: configuration)
                
                let token = Token()
                token.username = username
                token.requestToken = requestToken
                token.sessionID = sessionID
                
                try self?.logInViewController.realm?.write {
                    self?.logInViewController.realm?.add(token)
                }
                
                DispatchQueue.main.async {
                    self?.authSingle?(.success(true))
                }
            } catch {
                self?.authSingle?(.failure(error))
                // oslog not available before ios 14
                //print(String(format: NSLocalizedString("Got Realm error %@", comment: ""), error.localizedDescription))
            }
        }
    }
}
