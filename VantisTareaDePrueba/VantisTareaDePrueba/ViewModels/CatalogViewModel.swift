//
//  CatalogViewModel.swift
//  VantisTareaDePrueba
//
//  Created by Nikolay Taran para VANTIS empresa on 22.04.2022.
//

import Foundation
import RxSwift

protocol CatalogViewModel {
    func showPopular() -> Single<[Movie]>
    func showTopRated() -> Single<[Movie]>
    func showOnTV() -> Single<[Movie]>
    func showAiringToday() -> Single<[Movie]>
    func cachedPoster(id: Int) -> Data?
}
