//
//  ProfileViewModel.swift
//  VantisTareaDePrueba
//
//  Created by Nikolay Taran para VANTIS empresa on 21.04.2022.
//

import Foundation
import RxSwift

protocol ProfileViewModel {
    func updateProfile() -> Single<Data?>
    func cachedPoster(id: Int) -> Data?
}
