//
//  DetailsViewModelImplementation.swift
//  VantisTareaDePrueba
//
//  Created by Nikolay Taran para VANTIS empresa on 22.04.2022.
//

import Foundation
import RxSwift
import RealmSwift

class DetailsViewModelImplementation: NSObject, DetailsViewModel {
    
    // MARK: - Properties
    
    let detailsViewController: DetailsViewController
    
    private let disposeBag = DisposeBag()
    
    private var detailsSingle: ((Result<Data?, Error>) -> Void)?
    
    init(detailsViewController: DetailsViewController) {
        self.detailsViewController = detailsViewController
    }
    
    func updateBackdrop(backdropPath: String, id: Int) -> Single<Data?> {
        return Single<Data?>.create { [weak self] single in
            if let cachedBackdrop = self?.cachedBackdrop(id: id) {
                single(.success(cachedBackdrop))
            } else {
                self?.detailsSingle = single
                
                let backdropURL = "https://image.tmdb.org/t/p/original" + backdropPath
                self?.requestBackdrop(backdropURL: backdropURL, id: id)
            }
            return Disposables.create()
        }
    }
    
    func loadBackdrop(backdropURL: String) -> Single<Data> {
        return Single<Data>.create { single in
            if let url = URL(string: backdropURL) {
                let task = URLSession.shared.dataTask(with: url) { data, response, error in
                    if let data = data {
                        single(.success(data))
                    } else {
                        let error = NSError(domain: "com.vantis.error",
                                            code: 0,
                                            userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Empty data", comment: "")])
                        single(.failure(error))
                    }
                }
                task.resume()
            } else {
                let error = NSError(domain: "com.vantis.error",
                                    code: 0,
                                    userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Wrong URL", comment: "")])
                single(.failure(error))
            }
            
            return Disposables.create()
        }
    }
    
    func cachedBackdrop(id: Int) -> Data? {
        let tempDirectory = FileManager.default.temporaryDirectory.appendingPathComponent("VantisExam", isDirectory: true)
        if !FileManager.default.fileExists(atPath: tempDirectory.relativePath) {
            return nil
        }
        
        let backdropFile = tempDirectory.appendingPathComponent("backdrop\(id).jpg", isDirectory: false)
        return try? Data(contentsOf: backdropFile)
    }
    
    func saveBackdrop(data: Data, id: Int) {
        do {
            let tempDirectory = FileManager.default.temporaryDirectory.appendingPathComponent("VantisExam", isDirectory: true)
            if !FileManager.default.fileExists(atPath: tempDirectory.relativePath) {
                try FileManager.default.createDirectory(at: tempDirectory,
                                                         withIntermediateDirectories: true,
                                                         attributes: nil)
            }
            
            let backdropFile = tempDirectory.appendingPathComponent("backdrop\(id).jpg", isDirectory: false)
            try data.write(to: backdropFile)
            
            // oslog not available before ios 14
            print("Avatar saved successfully")
            //logger.debug("Avatar saved successfully")
        } catch {
            // oslog not available before ios 14
            print(error)
        }
    }
    
    func requestBackdrop(backdropURL: String, id: Int) {
        self.loadBackdrop(backdropURL: backdropURL).subscribe(onSuccess: {data in
            DispatchQueue.main.async { [weak self] in
                if UIImage(data: data) != nil {
                    self?.saveBackdrop(data: data, id: id)
                    
                    // oslog not available before ios 14
                    //self?.logger.debug("image loaded")
                    print("image loaded")
                } else {
                    // oslog not available before ios 14
                    print("no image")
                    //self?.logger.debug("no image")
                }
                
                self?.detailsSingle?(.success(data))
            }
        }, onFailure: { [weak self] error in
            self?.detailsSingle?(.failure(error))
        }, onDisposed: {
        }).disposed(by: disposeBag)
    }
    
    func changeFavoriteStatus(isFavorite: Bool, id: Int) -> Single<Bool> {
        return Single<Bool>.create { single in
            OperationQueue().addOperation { [weak self] in
                do {
                    var configuration = Realm.Configuration()
                    configuration.deleteRealmIfMigrationNeeded = true
                    self?.detailsViewController.realm = try Realm(configuration: configuration)
                    
                    try self?.detailsViewController.realm?.write {
                        if isFavorite {
                            let favorite = FavoriteMovie()
                            favorite.id = id
                            self?.detailsViewController.realm?.add(favorite)
                        } else {
                            let favorites = self?.detailsViewController.realm?.objects(FavoriteMovie.self).filter("id = %@", id)
                            if let favorites = favorites {
                                self?.detailsViewController.realm?.delete(favorites)
                            }
                        }
                    }
                    
                    single(.success(true))
                } catch {
                    single(.failure(error))
                }
            }
            
            return Disposables.create()
        }
    }
    
    func checkIfFavorite() -> Single<Bool> {
        return Single<Bool>.create { single in
            OperationQueue().addOperation { [weak self] in
                do {
                    var configuration = Realm.Configuration()
                    configuration.deleteRealmIfMigrationNeeded = true
                    self?.detailsViewController.realm = try Realm(configuration: configuration)
                    
                    let favorites = self?.detailsViewController.realm?.objects(FavoriteMovie.self)
                    if let id = self?.detailsViewController.movie?.id, let favorites = favorites {
                        
                        var isFavorite = false
                        
                        for favoriteMovie in favorites {
                            if favoriteMovie.id == id {
                                isFavorite = true
                                break
                            }
                        }
                        
                        single(.success(isFavorite))
                    } else {
                        single(.success(false))
                    }
                } catch {
                    single(.failure(error))
                }
            }
            
            return Disposables.create()
        }
    }
}
