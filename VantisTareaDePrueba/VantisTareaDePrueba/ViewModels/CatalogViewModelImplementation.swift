//
//  CatalogViewModelImplementation.swift
//  VantisTareaDePrueba
//
//  Created by Nikolay Taran para VANTIS empresa on 22.04.2022.
//

import Foundation
import RxSwift
import RealmSwift
import SwiftyJSON

class CatalogViewModelImplementation: NSObject, CatalogViewModel {
    
    // MARK: - Properties
    
    let catalogViewController: CatalogViewController
    
    private let disposeBag = DisposeBag()
    
    private var catalogSingle: ((Result<[Movie], Error>) -> Void)?
    
    init(catalogViewController: CatalogViewController) {
        self.catalogViewController = catalogViewController
    }
    
    func showPopular() -> Single<[Movie]> {
        return Single<[Movie]>.create { [weak self] single in
            self?.catalogSingle = single
            self?.requestPopular()
            return Disposables.create()
        }
    }
    
    func showTopRated() -> Single<[Movie]> {
        return Single<[Movie]>.create { [weak self] single in
            self?.catalogSingle = single
            self?.requestTopRated()
            return Disposables.create()
        }
    }
    
    func showOnTV() -> Single<[Movie]> {
        return Single<[Movie]>.create { [weak self] single in
            self?.catalogSingle = single
            self?.requestOnTV()
            return Disposables.create()
        }
    }
    
    func showAiringToday() -> Single<[Movie]> {
        return Single<[Movie]>.create { [weak self] single in
            self?.catalogSingle = single
            self?.requestAiringToday()
            return Disposables.create()
        }
    }
    
    // MARK: - Posters
    
    func loadPoster(posterURL: String) -> Single<Data> {
        return Single<Data>.create { single in
            if let url = URL(string: posterURL) {
                let task = URLSession.shared.dataTask(with: url) { data, response, error in
                    if let data = data {
                        single(.success(data))
                    } else {
                        let error = NSError(domain: "com.vantis.error",
                                            code: 0,
                                            userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Empty data", comment: "")])
                        single(.failure(error))
                    }
                }
                task.resume()
            } else {
                let error = NSError(domain: "com.vantis.error",
                                    code: 0,
                                    userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Wrong URL", comment: "")])
                single(.failure(error))
            }
            
            return Disposables.create()
        }
    }
    
    func cachedPoster(id: Int) -> Data? {
        let tempDirectory = FileManager.default.temporaryDirectory.appendingPathComponent("VantisExam", isDirectory: true)
        if !FileManager.default.fileExists(atPath: tempDirectory.relativePath) {
            return nil
        }
        
        let posterFile = tempDirectory.appendingPathComponent("poster\(id).jpg", isDirectory: false)
        return try? Data(contentsOf: posterFile)
    }
    
    func savePoster(data: Data, id: Int) {
        do {
            let tempDirectory = FileManager.default.temporaryDirectory.appendingPathComponent("VantisExam", isDirectory: true)
            if !FileManager.default.fileExists(atPath: tempDirectory.relativePath) {
                try FileManager.default.createDirectory(at: tempDirectory,
                                                         withIntermediateDirectories: true,
                                                         attributes: nil)
            }
            
            let posterFile = tempDirectory.appendingPathComponent("poster\(id).jpg", isDirectory: false)
            try data.write(to: posterFile)
            
            // oslog not available before ios 14
            print("Poster saved successfully")
            //logger.debug("Avatar saved successfully")
        } catch {
            // oslog not available before ios 14
            print(error)
        }
    }
    
    func requestPoster(posterURL: String,
                       downloadGroup: DispatchGroup,
                       id: Int) {
        self.loadPoster(posterURL: posterURL).subscribe(onSuccess: {data in
            DispatchQueue.main.async { [weak self] in
                if UIImage(data: data) != nil {
                    self?.savePoster(data: data, id: id)
                    
                    // oslog not available before ios 14
                    //self?.logger.debug("image loaded")
                    print("image loaded")
                } else {
                    // oslog not available before ios 14
                    print("no image")
                    //self?.logger.debug("no image")
                }
                
                downloadGroup.leave()
            }
        }, onFailure: { error in
            downloadGroup.leave()
        }, onDisposed: {
        }).disposed(by: disposeBag)
    }
    
    private func loadPosters(movies: [Movie]) {
        let downloadGroup = DispatchGroup()
        
        for movie in movies {
            if let posterPath = movie.posterPath, let id = movie.id {
                if cachedPoster(id: id) == nil {
                    let posterURL = "https://image.tmdb.org/t/p/original" + posterPath
                    
                    downloadGroup.enter()
                    requestPoster(posterURL: posterURL,
                                  downloadGroup: downloadGroup,
                                  id: id)
                }
            }
        }
        
        downloadGroup.notify(queue: .main) { [weak self] in
            self?.saveMoviesToCache(movies: movies)
        }
    }
    
    // MARK: - Request
    
    private func requestPopular() {
        loadPopular().subscribe(onSuccess: { [weak self] movies in
            self?.loadPosters(movies: movies)
        }, onFailure: { [weak self] error in
            self?.catalogSingle?(.failure(error))
        }, onDisposed: {
        }).disposed(by: disposeBag)
    }
    
    private func requestTopRated() {
        loadTopRated().subscribe(onSuccess: { [weak self] movies in
            self?.loadPosters(movies: movies)
        }, onFailure: { [weak self] error in
            self?.catalogSingle?(.failure(error))
        }, onDisposed: {
        }).disposed(by: disposeBag)
    }
    
    private func requestOnTV() {
        loadOnTV().subscribe(onSuccess: { [weak self] movies in
            self?.loadPosters(movies: movies)
        }, onFailure: { [weak self] error in
            self?.catalogSingle?(.failure(error))
        }, onDisposed: {
        }).disposed(by: disposeBag)
    }
    
    private func requestAiringToday() {
        loadAiringToday().subscribe(onSuccess: { [weak self] movies in
            self?.loadPosters(movies: movies)
        }, onFailure: { [weak self] error in
            self?.catalogSingle?(.failure(error))
        }, onDisposed: {
        }).disposed(by: disposeBag)
    }
    
    // MARK: - Networking
    
    private func loadPopular() -> Single<[Movie]> {
        let apiKey = Bundle.main.object(forInfoDictionaryKey: "TMDB API Key") as? String
        return Single<[Movie]>.create { single in
            if let apiKey = apiKey {
                let tokenURL = "https://api.themoviedb.org/3/tv/popular?api_key=\(apiKey)&language=en-US&page=10"
                
                if let url = URL(string: tokenURL) {
                    let task = URLSession.shared.dataTask(with: url) { data, response, error in
                        if let data = data, let json = try? JSON(data: data) {
                            
                            let results = json["results"].array
                            if let results = results {
                                var moviesArray: [Movie] = []
                                
                                for movieJSON in results {
                                    let movie = self.convertInto(movieJSON: movieJSON)
                                    moviesArray.append(movie)
                                }
                                
                                single(.success(moviesArray))
                            } else {
                                let error = NSError(domain: "com.vantis.error",
                                                    code: 0,
                                                    userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Empty data", comment: "")])
                                single(.failure(error))
                            }
                        } else if let error = error {
                            single(.failure(error))
                        } else {
                            let error = NSError(domain: "com.vantis.error",
                                                code: 0,
                                                userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Empty data", comment: "")])
                            single(.failure(error))
                        }
                    }
                    task.resume()
                } else {
                    let error = NSError(domain: "com.vantis.error",
                                        code: 0,
                                        userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Wrong URL", comment: "")])
                    single(.failure(error))
                }
            } else {
                let error = NSError(domain: "com.vantis.error",
                                    code: 0,
                                    userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Wrong URL", comment: "")])
                single(.failure(error))
            }
            
            return Disposables.create()
        }
    }
    
    private func loadTopRated() -> Single<[Movie]> {
        let apiKey = Bundle.main.object(forInfoDictionaryKey: "TMDB API Key") as? String
        return Single<[Movie]>.create { single in
            if let apiKey = apiKey {
                let tokenURL = "https://api.themoviedb.org/3/tv/top_rated?api_key=\(apiKey)&language=en-US&page=10"
                
                if let url = URL(string: tokenURL) {
                    let task = URLSession.shared.dataTask(with: url) { data, response, error in
                        if let data = data, let json = try? JSON(data: data) {
                            
                            let results = json["results"].array
                            if let results = results {
                                var moviesArray: [Movie] = []
                                
                                for movieJSON in results {
                                    let movie = self.convertInto(movieJSON: movieJSON)
                                    moviesArray.append(movie)
                                }
                                
                                single(.success(moviesArray))
                            } else {
                                let error = NSError(domain: "com.vantis.error",
                                                    code: 0,
                                                    userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Empty data", comment: "")])
                                single(.failure(error))
                            }
                        } else if let error = error {
                            single(.failure(error))
                        } else {
                            let error = NSError(domain: "com.vantis.error",
                                                code: 0,
                                                userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Empty data", comment: "")])
                            single(.failure(error))
                        }
                    }
                    task.resume()
                } else {
                    let error = NSError(domain: "com.vantis.error",
                                        code: 0,
                                        userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Wrong URL", comment: "")])
                    single(.failure(error))
                }
            } else {
                let error = NSError(domain: "com.vantis.error",
                                    code: 0,
                                    userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Wrong URL", comment: "")])
                single(.failure(error))
            }
            
            return Disposables.create()
        }
    }
    
    private func loadOnTV() -> Single<[Movie]> {
        let apiKey = Bundle.main.object(forInfoDictionaryKey: "TMDB API Key") as? String
        return Single<[Movie]>.create { single in
            if let apiKey = apiKey {
                let tokenURL = "https://api.themoviedb.org/3/tv/on_the_air?api_key=\(apiKey)&language=en-US&page=10"
                
                if let url = URL(string: tokenURL) {
                    let task = URLSession.shared.dataTask(with: url) { data, response, error in
                        if let data = data, let json = try? JSON(data: data) {
                            
                            let results = json["results"].array
                            if let results = results {
                                var moviesArray: [Movie] = []
                                
                                for movieJSON in results {
                                    let movie = self.convertInto(movieJSON: movieJSON)
                                    moviesArray.append(movie)
                                }
                                
                                single(.success(moviesArray))
                            } else {
                                let error = NSError(domain: "com.vantis.error",
                                                    code: 0,
                                                    userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Empty data", comment: "")])
                                single(.failure(error))
                            }
                        } else if let error = error {
                            single(.failure(error))
                        } else {
                            let error = NSError(domain: "com.vantis.error",
                                                code: 0,
                                                userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Empty data", comment: "")])
                            single(.failure(error))
                        }
                    }
                    task.resume()
                } else {
                    let error = NSError(domain: "com.vantis.error",
                                        code: 0,
                                        userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Wrong URL", comment: "")])
                    single(.failure(error))
                }
            } else {
                let error = NSError(domain: "com.vantis.error",
                                    code: 0,
                                    userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Wrong URL", comment: "")])
                single(.failure(error))
            }
            
            return Disposables.create()
        }
    }
    
    private func loadAiringToday() -> Single<[Movie]> {
        let apiKey = Bundle.main.object(forInfoDictionaryKey: "TMDB API Key") as? String
        return Single<[Movie]>.create { single in
            if let apiKey = apiKey {
                let tokenURL = "https://api.themoviedb.org/3/tv/airing_today?api_key=\(apiKey)&language=en-US&page=10"
                
                if let url = URL(string: tokenURL) {
                    let task = URLSession.shared.dataTask(with: url) { data, response, error in
                        if let data = data, let json = try? JSON(data: data) {
                            
                            let results = json["results"].array
                            if let results = results {
                                var moviesArray: [Movie] = []
                                
                                for movieJSON in results {
                                    let movie = self.convertInto(movieJSON: movieJSON)
                                    moviesArray.append(movie)
                                }
                                
                                single(.success(moviesArray))
                            } else {
                                let error = NSError(domain: "com.vantis.error",
                                                    code: 0,
                                                    userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Empty data", comment: "")])
                                single(.failure(error))
                            }
                        } else if let error = error {
                            single(.failure(error))
                        } else {
                            let error = NSError(domain: "com.vantis.error",
                                                code: 0,
                                                userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Empty data", comment: "")])
                            single(.failure(error))
                        }
                    }
                    task.resume()
                } else {
                    let error = NSError(domain: "com.vantis.error",
                                        code: 0,
                                        userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Wrong URL", comment: "")])
                    single(.failure(error))
                }
            } else {
                let error = NSError(domain: "com.vantis.error",
                                    code: 0,
                                    userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Wrong URL", comment: "")])
                single(.failure(error))
            }
            
            return Disposables.create()
        }
    }
    
    private func convertInto(movieJSON: JSON) -> Movie {
        let movie = Movie()
        
        movie.backdropPath = movieJSON["backdrop_path"].string
        movie.firstAirDate = movieJSON["first_air_date"].string
        movie.id = movieJSON["id"].int
        movie.name = movieJSON["name"].string
        movie.originalLanguage = movieJSON["original_language"].string
        movie.originalName = movieJSON["original_name"].string
        movie.overview = movieJSON["overview"].string
        movie.popularity = movieJSON["popularity"].double
        movie.posterPath = movieJSON["poster_path"].string
        movie.voteAverage = movieJSON["vote_average"].double
        movie.voteCount = movieJSON["vote_count"].int
        
        return movie
    }
    
    private func clone(existingMovie: Movie) -> Movie {
        let movie = Movie()
        
        movie.backdropPath = existingMovie.backdropPath
        movie.firstAirDate = existingMovie.firstAirDate
        movie.id = existingMovie.id
        movie.name = existingMovie.name
        movie.originalLanguage = existingMovie.originalLanguage
        movie.originalName = existingMovie.originalName
        movie.overview = existingMovie.overview
        movie.popularity = existingMovie.popularity
        movie.posterPath = existingMovie.posterPath
        movie.voteAverage = existingMovie.voteAverage
        movie.voteCount = existingMovie.voteCount
        
        return movie
    }
    
    // MARK: - Database
    
    private func saveMoviesToCache(movies: [Movie]) {
        OperationQueue().addOperation { [weak self] in
            do {
                var configuration = Realm.Configuration()
                configuration.deleteRealmIfMigrationNeeded = true
                self?.catalogViewController.realm = try Realm(configuration: configuration)
                
                try self?.catalogViewController.realm?.write {
                    for movie in movies {
                        if let id = movie.id {
                            if let existingMovies = self?.catalogViewController.realm?.objects(Movie.self).filter("id = %@", id) {
                                self?.catalogViewController.realm?.delete(existingMovies)
                            }
                        
                            if let clone = self?.clone(existingMovie: movie) {
                                self?.catalogViewController.realm?.add(clone)
                            }
                        }
                    }
                }
                
                DispatchQueue.main.async {
                    self?.catalogSingle?(.success(movies))
                }
            } catch {
                self?.catalogSingle?(.failure(error))
                // oslog not available before ios 14
                //print(String(format: NSLocalizedString("Got Realm error %@", comment: ""), error.localizedDescription))
            }
        }
    }
}
