//
//  DetailsViewModel.swift
//  VantisTareaDePrueba
//
//  Created by Nikolay Taran para VANTIS empresa on 22.04.2022.
//

import Foundation
import RxSwift

protocol DetailsViewModel {
    func updateBackdrop(backdropPath: String, id: Int) -> Single<Data?>
    func checkIfFavorite() -> Single<Bool>
    func changeFavoriteStatus(isFavorite: Bool, id: Int) -> Single<Bool>
}
