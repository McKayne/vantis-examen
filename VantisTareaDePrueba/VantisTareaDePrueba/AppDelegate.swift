//
//  AppDelegate.swift
//  VantisTareaDePrueba
//
//  Created by Nikolay Taran para VANTIS empresa on 19.04.2022.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().tintColor = .white
        
        let backgroundColor = UIColor(red: 50.0 / 255.0,
                                      green: 55.0 / 255.0,
                                      blue: 60.0 / 255.0,
                                      alpha: 1.0)
        let font = UIFont.systemFont(ofSize: 20,
                                     weight: .heavy)
        if #available(iOS 13.0, *) {
            let barAppearance = UINavigationBarAppearance()
            barAppearance.backgroundColor = backgroundColor
            barAppearance.titleTextAttributes = [.foregroundColor: UIColor.white, .font: font]
            
            let navigationBar = UINavigationBar.appearance()
            navigationBar.standardAppearance = barAppearance
            navigationBar.scrollEdgeAppearance = barAppearance
        } else {
            UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: UIColor.white, .font: font]
            UINavigationBar.appearance().barTintColor = backgroundColor
        }
        
        
        
        //UINavigationBar.appearance().barTintColor = UIColor.cyan
        
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
        return true
    }
}

