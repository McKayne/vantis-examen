//
//  CatalogViewCell.swift
//  VantisTareaDePrueba
//
//  Created by Nikolay Taran para VANTIS empresa on 21.04.2022.
//

import Foundation
import UIKit

class CatalogViewCell: UICollectionViewCell {
    
    let posterImageView = UIImageView()
    
    let nameLabel = UILabel()
    
    let voteAverageLabel = UILabel()
    
    let descriptionLabel = UILabel()
    
    let cellBackgroundView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateCell(poster: UIImage?, movie: Movie) {
        posterImageView.image = poster
        posterImageView.setNeedsDisplay()
        
        nameLabel.text = movie.name
        
        if let voteAverage = movie.voteAverage {
            voteAverageLabel.text = "\(voteAverage) of 10"
        }
        
        descriptionLabel.text = movie.overview
    }
    
    private func addDescription() {
        descriptionLabel.text = ""
        descriptionLabel.textAlignment = .justified
        descriptionLabel.numberOfLines = 5
        descriptionLabel.textColor = .white
        descriptionLabel.font = UIFont.systemFont(ofSize: 10,
                                           weight: .medium)
        
        cellBackgroundView.addSubview(descriptionLabel)
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.leftAnchor.constraint(equalTo: cellBackgroundView.leftAnchor, constant: 5).isActive = true
        descriptionLabel.rightAnchor.constraint(equalTo: cellBackgroundView.rightAnchor, constant: -5).isActive = true
        
        let topConstraint = descriptionLabel.topAnchor.constraint(equalTo: voteAverageLabel.bottomAnchor, constant: 5)
        topConstraint.priority = UILayoutPriority(222)
        topConstraint.isActive = true
        
        let bottomConstraint = descriptionLabel.bottomAnchor.constraint(equalTo: cellBackgroundView.bottomAnchor, constant: -5)
        bottomConstraint.priority = UILayoutPriority(111)
            
        bottomConstraint.isActive = true
    }
    
    private func addVoteAverage() {
        voteAverageLabel.text = ""
        voteAverageLabel.textColor = .white
        voteAverageLabel.font = UIFont.systemFont(ofSize: 20,
                                           weight: .medium)
        
        cellBackgroundView.addSubview(voteAverageLabel)
        voteAverageLabel.translatesAutoresizingMaskIntoConstraints = false
        voteAverageLabel.leftAnchor.constraint(equalTo: cellBackgroundView.leftAnchor, constant: 5).isActive = true
        voteAverageLabel.rightAnchor.constraint(equalTo: cellBackgroundView.rightAnchor, constant: -5).isActive = true
        voteAverageLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 5).isActive = true
    }
    
    private func addCellBackground() {
        cellBackgroundView.backgroundColor = UIColor(red: 50.0 / 255.0,
                                                     green: 55.0 / 255.0,
                                                     blue: 60.0 / 255.0,
                                                     alpha: 1.0)
        cellBackgroundView.clipsToBounds = true
        contentView.addSubview(cellBackgroundView)
        
        cellBackgroundView.translatesAutoresizingMaskIntoConstraints = false
        cellBackgroundView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 0).isActive = true
        cellBackgroundView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: 0).isActive = true
        cellBackgroundView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0).isActive = true
        cellBackgroundView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0).isActive = true
        
        cellBackgroundView.layer.cornerRadius = 15
    }
    
    func addPoster() {
        posterImageView.contentMode = .scaleAspectFill
        posterImageView.clipsToBounds = true
        cellBackgroundView.addSubview(posterImageView)
        
        posterImageView.translatesAutoresizingMaskIntoConstraints = false
        posterImageView.leftAnchor.constraint(equalTo: cellBackgroundView.leftAnchor, constant: 0).isActive = true
        posterImageView.rightAnchor.constraint(equalTo: cellBackgroundView.rightAnchor, constant: 0).isActive = true
        posterImageView.topAnchor.constraint(equalTo: cellBackgroundView.topAnchor, constant: 0).isActive = true
        posterImageView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height / 6).isActive = true
        
        posterImageView.layer.cornerRadius = 15
    }
    
    func addName() {
        nameLabel.text = ""
        //nameLabel.numberOfLines = 0
        nameLabel.textColor = .white
        nameLabel.font = UIFont.systemFont(ofSize: 20,
                                           weight: .bold)
        
        cellBackgroundView.addSubview(nameLabel)
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.leftAnchor.constraint(equalTo: cellBackgroundView.leftAnchor, constant: 5).isActive = true
        nameLabel.rightAnchor.constraint(equalTo: cellBackgroundView.rightAnchor, constant: -5).isActive = true
        
        let constraint = nameLabel.topAnchor.constraint(equalTo: posterImageView.bottomAnchor, constant: 5)
        constraint.priority = UILayoutPriority(999)
        constraint.isActive = true
    }
    
    func addViews() {
        addCellBackground()
       
        addPoster()
        
        addName()
        
        addVoteAverage()
        
        addDescription()
    }
}
