//
//  UIImage+Extensions.swift
//  VantisTareaDePrueba
//
//  Created by Nikolay Taran para VANTIS empresa on 22.04.2022.
//

import Foundation
import UIKit

extension UIImage {
    
    func imageResized(to size: CGSize) -> UIImage {
        return UIGraphicsImageRenderer(size: size).image { _ in
            draw(in: CGRect(origin: .zero, size: size))
        }
    }
}
